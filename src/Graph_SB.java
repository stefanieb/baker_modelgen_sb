import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.kernel.api.exceptions.schema.UniqueConstraintViolationKernelException;

import ca.uwaterloo.cs.se.inconsistency.core.model2.ClassElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.FieldElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.MethodElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.MethodParamElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.Model;
import ca.uwaterloo.cs.se.inconsistency.core.model2.io.Model2XMLReader;

public class Graph_SB {
	private static Model _model;
	private static final String DB_PATH = "/Users/Stefanie/Documents/workspace_baker/db/graph.db";
	private static GraphDatabaseService graphDb;
	private static Index<Node> nodeIndexClass;
	private static Index<Node> nodeIndexMethod;
	private static Index<Node> nodeIndexField;
	private static Index<Node> nodeIndexShortField;
	private static Index<Node> nodeIndexShortMethod;
	private static Index<Node> nodeIndexShortClass;

	private static double version = -1.0;

	private static final String PATH_TO_XML = "/Users/Stefanie/Documents/workspace_baker/android_jars/";

	private static Index<Node> nodeParents;

	private static enum RelTypes implements RelationshipType {
		PARENT, CHILD, IS_METHOD, HAS_METHOD, IS_FIELD, HAS_FIELD, RETURN_TYPE, IS_RETURN_TYPE, PARAMETER, IS_PARAMETER, IS_FIELD_TYPE, HAS_FIELD_TYPE
	}

	public static void populate(String fName) throws IOException {
		// System.out.println("Processing XML: " + fName);
		Model2XMLReader xmlrdf = new Model2XMLReader(fName);
		Model knownModel = xmlrdf.read();
		_model = knownModel;
		Node node = null;
		System.out.println("da liegt der hund begraben!");
		for (ClassElement ce : knownModel.getClasses()) {
			try {
				node = createAndIndexClassElement(ce);
			} catch (ConstraintViolationException e) {
				System.out.println("class already exists");
			}
			for (MethodElement me : ce.getMethods()) {
				try {
					createAndIndexMethodElement(me, ce);
			} catch (ConstraintViolationException e) {
				System.out.println("method already exists");
				}
			}
			for (FieldElement fe : ce.getFields()) {
				try {
					createAndIndexFieldElement(fe, ce);
				} catch (ConstraintViolationException e) {
					System.out.println("field already exists");
				}
			}
			if (node != null) {
				try {
				createAndIndexParents(ce, node);
				} catch (ConstraintViolationException e) {
					System.out.println("index and parents already exists");
				}
			}
		}
	}

	public static void main(final String[] args) throws IOException {
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		try (Transaction tx = graphDb.beginTx()) {
			nodeIndexClass = graphDb.index().forNodes("classes");
			nodeIndexMethod = graphDb.index().forNodes("methods");
			nodeIndexField = graphDb.index().forNodes("fields");
			nodeIndexShortField = graphDb.index().forNodes("short_fields");
			nodeIndexShortMethod = graphDb.index().forNodes("short_methods");
			nodeIndexShortClass = graphDb.index().forNodes("short_classes");
			nodeParents = graphDb.index().forNodes("parents");

			//constraints			
//			graphDb.schema().constraintFor(DynamicLabel.label("class")).assertPropertyIsUnique("uniqueness").create();
//			graphDb.schema().constraintFor(DynamicLabel.label("method")).assertPropertyIsUnique("uniqueness").create();
//			graphDb.schema().constraintFor(DynamicLabel.label("field")).assertPropertyIsUnique("uniqueness").create();
//			graphDb.schema().constraintFor(DynamicLabel.label("param")).assertPropertyIsUnique("uniqueness").create();

			registerShutdownHook();
			tx.success(); // mostly needed for write operations but also doesn't
							// hurt for read ops
		}

		// Uncomment for a single XML to be appended to the graph
		/// *
		// String fName2 =
		// "/Users/Stefanie/Documents/workspace_baker/maven_data/xml/a.xml";
		// Transaction tx0 = graphDb.beginTx();
		// try {
		// populate(fName2);
		// tx0.success();
		// }
		//
		// finally {
		// tx0.close();
		//// tx0.finish();
		// }
		// */
		// Uncomment to iterate over all XML files in a directory

		File xmlPath = new File(PATH_TO_XML);
		File[] fileList = xmlPath.listFiles();
		int i = 0;
		//boolean order = false;
		for (File file : fileList) {
			i++;
			if (i >= 0) {
				String fname = file.getAbsolutePath();

				Transaction tx1 = graphDb.beginTx();
				try { // if(fname.contains("general_")==true)
					if ((fname.endsWith("19.xml"))) {
//					if ((fname.endsWith("19.xml"))|| (fname.endsWith("17.xml"))) {
//					if ((fname.endsWith("19.xml") && !order)|| (fname.endsWith("17.xml") && order)) {
					//	order = true;
						System.out.println("Processing " + fname + " : " + i);

						version = getVersionNumber(fname);
						System.out.println("Version: " + version);
						populate(fname);
						tx1.success();
					}
				} finally {
					tx1.close();
					// tx1.finish();
				}
			}
		}

		shutdown();
	}

	/**
	 * @author steffi
	 * @param filename
	 * @return
	 */
	private static Double getVersionNumber(String filename) {

		Double version = -1.0;
		Pattern pattern = Pattern.compile("\\d\\d?(\\.\\d+)?");
		Matcher matcher = pattern.matcher(filename);
		if (matcher.find()) {
			version = Double.valueOf(matcher.group());
		}
		return version;

	}

	private static void shutdown() {
		graphDb.shutdown();
	}

	private static void createAndIndexParents(ClassElement ce, Node node) throws IOException {
		Collection<ClassElement> parentsList = ce.getParents();
		if (parentsList != null) {
			for (ClassElement parent : parentsList) {
				Node parentNode = createAndIndexClassElement(parent);
				node.createRelationshipTo(parentNode, RelTypes.PARENT);
				parentNode.createRelationshipTo(node, RelTypes.CHILD);
				nodeParents.add(parentNode, "parent", ce.getId());
			}
		}
	}

	private static Node createAndIndexClassElement(ClassElement ce) throws IOException {
		IndexHits<Node> userNodes = nodeIndexClass.get("id", ce.getId()+version);
		Iterator<Node> iter = userNodes.iterator();
		if (iter.hasNext() == false) {
			Node node = graphDb.createNode();
			node.setProperty("id", ce.getId()+version);
			node.setProperty("fullName", ce.getId());
			node.setProperty("exactName", ce.getExactName());
			node.setProperty("vis", ce.getVisiblity().toString());
			node.setProperty("isAbstract", ce.isAbstract());
			node.setProperty("isPrimitive", "false");
			node.setProperty("isInterface", ce.isInterface());
			node.setProperty("isExternal", ce.isExternal());
			node.setProperty("version", version);

			node.setProperty("uniqueness", getUniquenessNode(node));

			Label myLabel = DynamicLabel.label("class");
			node.addLabel(myLabel);

			nodeIndexClass.add(node, "id", ce.getId()+version);
			nodeIndexShortClass.add(node, "short_name", ce.getExactName());
			return node;
		} else {
			Node existingNode = userNodes.getSingle();
			return existingNode;
		}
	}

	/**
	 * @author steffi returns unique key for node
	 * @param node
	 * @return string key
	 */
	private static String getUniquenessNode(Node node) {
		StringBuffer sb = new StringBuffer();
		sb.append(node.getProperty("fullName"));
		sb.append(node.getProperty("exactName"));
		sb.append(node.getProperty("vis"));
		sb.append(node.getProperty("isAbstract"));
		sb.append(node.getProperty("isPrimitive"));
		sb.append(node.getProperty("isInterface"));
		sb.append(node.getProperty("isExternal"));
		sb.append(node.getProperty("version"));

		return sb.toString();
	}

	/**
	 * @author steffi returns unique key for node
	 * @param node
	 * @return string key
	 */
	private static String getUniquenessParam(Node node) {
		StringBuffer sb = new StringBuffer();
		sb.append(node.getProperty("fullName"));
		sb.append(node.getProperty("exactName"));
		sb.append(node.getProperty("vis"));
		sb.append(node.getProperty("isAbstract"));
		sb.append(node.getProperty("isPrimitive"));
		sb.append(node.getProperty("isInterface"));
		sb.append(node.getProperty("isExternal"));
		sb.append(node.getProperty("version"));
		sb.append(node.getProperty("paramIndex"));

		return sb.toString();
	}

	/**
	 * @author steffi returns unique key for node
	 * @param node
	 * @return string key
	 */
	private static String getUniquenessMethod(Node node) {
		StringBuffer sb = new StringBuffer();
		sb.append(node.getProperty("fullName"));
		sb.append(node.getProperty("exactName"));
		sb.append(node.getProperty("vis"));
		sb.append(node.getProperty("version"));

		return sb.toString();
	}

	/**
	 * @author steffi returns unique key for node
	 * @param node
	 * @return string key
	 */
	private static String getUniquenessField(Node node) {
		StringBuffer sb = new StringBuffer();
		sb.append(node.getProperty("fullName"));
		sb.append(node.getProperty("exactName"));
		sb.append(node.getProperty("vis"));
		sb.append(node.getProperty("isPrimitive"));
		sb.append(node.getProperty("isExternal"));
		sb.append(node.getProperty("version"));

		return sb.toString();
	}

	private static Node createAndIndexMethodElement(MethodElement me, ClassElement ce)
			throws IOException, ConstraintViolationException {
		IndexHits<Node> methodNodes = nodeIndexMethod.get("id", me.getId()+version);
		if (methodNodes.hasNext() == false) {
			// System.out.println(me.getId());
			Node node = graphDb.createNode();
			node.setProperty("id", me.getId()+version);
			node.setProperty("fullName", me.getId());
			node.setProperty("exactName", me.getExactName());
			node.setProperty("vis", me.getVisiblity().toString());

			node.setProperty("version", version);
			node.setProperty("uniqueness", getUniquenessMethod(node));

			Label myLabel = DynamicLabel.label("method");
			node.addLabel(myLabel);

			ClassElement parentClass = ce;
			insertParentAndReturn(RelTypes.IS_METHOD, RelTypes.HAS_METHOD, parentClass, node);

			ClassElement returnType = me.getReturnElement().getType();
			insertParentAndReturn(RelTypes.RETURN_TYPE, RelTypes.IS_RETURN_TYPE, returnType, node);

			node.setProperty("argCount", me.getParameters().size());
			Collection<MethodParamElement> params = me.getParameters();
			int i = 0;
			for (MethodParamElement param : params) {
				i++;
				ClassElement paramtype = param.getType();
				insertParameter(RelTypes.PARAMETER, RelTypes.IS_PARAMETER, paramtype, node, i);
			}

			nodeIndexMethod.add(node, "id", me.getId()+version);
			nodeIndexShortMethod.add(node, "short_name", me.getExactName());
			return node;
		} else
			return methodNodes.getSingle();
	}

	private static Node createAndIndexFieldElement(FieldElement fe, ClassElement ce) throws IOException {
		IndexHits<Node> fieldNodes = nodeIndexField.get("id", fe.getId()+version);
		if (fieldNodes.hasNext() == false) {
			// System.out.println(ce.getId());
			Node node = graphDb.createNode();
			node.setProperty("id", fe.getId()+version);
			node.setProperty("fullName", fe.getId());
			node.setProperty("exactName", fe.getExactName());
			node.setProperty("vis", fe.getVisiblity().toString());
			node.setProperty("isPrimitive", "false");
			node.setProperty("isExternal", fe.isExternal());
			node.setProperty("version", version);

			node.setProperty("uniqueness", getUniquenessField(node));

			Label myLabel = DynamicLabel.label("field");
			node.addLabel(myLabel);

			ClassElement fieldType = fe.getType();

			insertParentAndReturn(RelTypes.IS_FIELD_TYPE, RelTypes.HAS_FIELD_TYPE, fieldType, node);
			System.out.println(fe.getId());
			ClassElement parentClass = ce;
			insertParentAndReturn(RelTypes.IS_FIELD, RelTypes.HAS_FIELD, ce, node);

			/*
			 * byte[] array = fe.convertFieldElementToByteArray();
			 * //System.out.println(array.length);
			 * node.setProperty("FEByteArray", array);
			 */
			nodeIndexField.add(node, "id", fe.getId()+version);
			nodeIndexShortField.add(node, "short_name", fe.getExactName());
			return node;
		} else
			return fieldNodes.getSingle();
	}

	private static void insertParentAndReturn(RelationshipType outgoing, RelationshipType incoming, ClassElement type,
			Node node) throws IOException, ConstraintViolationException {
		if (type == null)
			return;
		// System.out.println(type.getId());
		IndexHits<Node> returnNode = nodeIndexClass.get("id", type.getId()+version);
		
		
		if (returnNode.hasNext()) {
			Node returnTypeNode = returnNode.getSingle();
			node.createRelationshipTo(returnTypeNode, outgoing);
			returnTypeNode.createRelationshipTo(node, incoming);
		} else if (Convert.isPrimitive(type.getId())) {
			Node primitiveNode = graphDb.createNode();
			primitiveNode.setProperty("id", type.getId()+version);
			primitiveNode.setProperty("fullName", type.getId());
			primitiveNode.setProperty("exactName", type.getExactName());
			
			primitiveNode.setProperty("vis", type.getVisiblity().toString());
			primitiveNode.setProperty("isAbstract", type.isAbstract());
			primitiveNode.setProperty("isInterface", type.isInterface());
			primitiveNode.setProperty("isExternal", type.isExternal());
			primitiveNode.setProperty("isPrimitive", "true");
			primitiveNode.setProperty("version", version);

			Label myLabel = DynamicLabel.label("class");
			primitiveNode.addLabel(myLabel);

			primitiveNode.setProperty("uniqueness", getUniquenessNode(primitiveNode));

			/*
			 * type.setParentNull(); type.setMethodsNull();
			 * type.setFieldsNull(); primitiveNode.setProperty("CEByteArray",
			 * type.convertClassElementToByteArray());
			 */
			node.createRelationshipTo(primitiveNode, outgoing);
			primitiveNode.createRelationshipTo(node, incoming);
		} else {
			Node newReturnNode = graphDb.createNode();
			newReturnNode.setProperty("id", type.getId()+version);
			newReturnNode.setProperty("fullName", type.getId());
			newReturnNode.setProperty("exactName", type.getExactName());
			newReturnNode.setProperty("vis", type.getVisiblity().toString());
			newReturnNode.setProperty("isAbstract", type.isAbstract());
			newReturnNode.setProperty("isInterface", type.isInterface());
			newReturnNode.setProperty("isExternal", type.isExternal());
			newReturnNode.setProperty("isPrimitive", "false");
			newReturnNode.setProperty("version", version);

			newReturnNode.setProperty("uniqueness", getUniquenessNode(newReturnNode));

			Label myLabel = DynamicLabel.label("class");
			newReturnNode.addLabel(myLabel);

			/*
			 * type.setParentNull(); type.setMethodsNull();
			 * type.setFieldsNull(); newReturnNode.setProperty("CEByteArray",
			 * type.convertClassElementToByteArray());
			 */
			node.createRelationshipTo(newReturnNode, outgoing);
			newReturnNode.createRelationshipTo(node, incoming);
		}
	}

	private static void insertParameter(RelationshipType outgoing, RelationshipType incoming, ClassElement type,
			Node node, int paramIndex) throws IOException, ConstraintViolationException {
		if (type == null)
			return;
		// System.out.println(type.getId());
		if (Convert.isPrimitive(type.getId())) {
			Node primitiveNode = graphDb.createNode();
			primitiveNode.setProperty("id", type.getId()+version);
			primitiveNode.setProperty("fullName", type.getId());
			primitiveNode.setProperty("exactName", type.getExactName());
			primitiveNode.setProperty("vis", type.getVisiblity().toString());
			primitiveNode.setProperty("isAbstract", type.isAbstract());
			primitiveNode.setProperty("isInterface", type.isInterface());
			primitiveNode.setProperty("isExternal", type.isExternal());
			primitiveNode.setProperty("isPrimitive", "true");
			primitiveNode.setProperty("paramIndex", paramIndex);
			primitiveNode.setProperty("version", version);

			primitiveNode.setProperty("uniqueness", getUniquenessParam(primitiveNode));

			Label myLabel = DynamicLabel.label("param");
			primitiveNode.addLabel(myLabel);
			/*
			 * type.setParentNull(); type.setMethodsNull();
			 * type.setFieldsNull(); primitiveNode.setProperty("CEByteArray",
			 * type.convertClassElementToByteArray());
			 */
			node.createRelationshipTo(primitiveNode, outgoing).setProperty("count", 0);
		} else {
			Node newReturnNode = graphDb.createNode();
			newReturnNode.setProperty("id", type.getId()+version);
			newReturnNode.setProperty("fullName", type.getId());
			newReturnNode.setProperty("exactName", type.getExactName());
			newReturnNode.setProperty("vis", type.getVisiblity().toString());
			newReturnNode.setProperty("isAbstract", type.isAbstract());
			newReturnNode.setProperty("isInterface", type.isInterface());
			newReturnNode.setProperty("isExternal", type.isExternal());
			newReturnNode.setProperty("isPrimitive", "false");

			/*
			 * type.setParentNull(); type.setMethodsNull();
			 * type.setFieldsNull(); newReturnNode.setProperty("CEByteArray",
			 * type.convertClassElementToByteArray());
			 */
			newReturnNode.setProperty("paramIndex", paramIndex);

			Label myLabel = DynamicLabel.label("param");
			newReturnNode.addLabel(myLabel);

			newReturnNode.setProperty("version", version);
			newReturnNode.setProperty("uniqueness", getUniquenessParam(newReturnNode));
			node.createRelationshipTo(newReturnNode, outgoing).setProperty("count", 0);
		}
	}

	private static void registerShutdownHook() {
		// Registers a shutdown hook for the Neo4j and index service instances
		// so that it shuts down nicely when the VM exits (even if you
		// "Ctrl-C" the running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				shutdown();
			}
		});
	}
}